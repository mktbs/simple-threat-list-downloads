#!/usr/bin/env python

import requests
import re

def main():

    url = "https://feeds.dshield.org/block.txt"
    list = requests.get(url)

    file = open('dshield.csv','w')
    file.write("dshield_networks" + "\n")

    for line in list.text.splitlines():
        try:
            line_match = re.match(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s*\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s*(\d{2})\s*",line,re.M|re.I)
            file.write(line_match.group(1) + "/" + line_match.group(2) + "\n")
        except:
            fail_condition = 1
    file.close()

if __name__== "__main__":
  main()
