#!/usr/bin/env python

import requests

def main():

    url = "https://check.torproject.org/torbulkexitlist"
    list = requests.get(url)
    file = open('tor_exit.csv','w')
    file.write("tor_exit_nodes" + "\n")
    file.write(list.text)
    file.close()

if __name__== "__main__":
  main()
