#!/usr/bin/env python

import requests
import re

def main():

    url = "https://feodotracker.abuse.ch/downloads/ipblocklist_recommended.txt"
    list = requests.get(url)

    file = open('abuse_ch.csv','w')
    file.write("abuse_ch_ips" + "\n")

    for line in list.text.splitlines():
        if re.search(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}",line):
            file.write(line + "\n")
    file.close()

if __name__== "__main__":
  main()
